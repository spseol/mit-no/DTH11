DHT11 (DHT22) senzor teploty a vlhosti
==========================================

![](datasheets/DHT11.png)

* Datašíty jsou [jeden](datasheets/DHT11-Temperature-Sensor.pdf) lepší než
[druhý](datasheets/DHT11-Technical-Data-Sheet-Translated-Version-1143054.pdf).



Jak číst data?
------------------------------------------------

![](datasheets/time_diagram.png)

Komunikaci zahajuje µprocesor tím, že stáhne datový vodič na 18 ms k zemi do úrovně L.
Senzor odpoví start-bitem (tedy impulzem, který setrvá v úrovni H cca 80 µs), za kterým
následuje 40 (`5x8=40`) datových bitů. 0 a 1 se rozlišuje podle toho jak dlouho setrvá
datový pin v úrovni H: cca 27 µs znamáná `0`; cca 70 µs znamená `1`; úroveň L trvá 
vždy 50 µs.

16 bitů představuje vlhkost, 16 bitů představuje teplotu a 8 bitů je kontrolní součet.


Schema zapojení
-------------------

![](datasheets/schema.png)

Stručná funkce programu
------------------------------------------------

* TRIGGER pin je [nastaven](src/main.c#L32) jeko výstup s otevřeným kolektorem.
* DATA pin je [nastaven](src/main.c#L25) jako vstup, který při 
  [vzestupné i setupné hraně](src/main.c#L27) generuje požadavek na obsluhu přerušení.

* [Pustím časovač](src/main.c#L36-38) a při každé hraně na pinu DATA si 
  [poznamenám](src/stm8s_it.c#L138-139) hodnotu jeho registru.

* Při [sestupné hraně](src/stm8s_it.c#L143) se podívám, jak dlouho trvala úroveň H a 
  [dekóduji](src/stm8s_it.c#L144-152) tak bit jako `0` nebo jako `1`.

* Opičárny s proměnnou [`times`](src/stm8s_it.c#L146) nejsou vůbec nutné: sloužilo to pouze jako ladění,
  abych se podíval, co trvá jak dlouho.

* Teplotu a vlhkost je poté třeba [vytáhnout](src/main.c#L105-109) z proměnné `data`.

* Cely proces je řízen ve stylu 
  ["stavový automat"](http://www.elektromys.eu/clanky/stm8_12_automat/clanek.html).



